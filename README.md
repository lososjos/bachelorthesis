# Introduction

This repository provides scripts used in my Bachelor's Thesis.

# Contents

Description of used code is provided here.

## cocosplit

This [cocosplit](https://github.com/akarazniewicz/cocosplit) git repository was used to split datasets. Closer
information
about this repository can be found in its own README.md.

## image-to-coco-json-converter

This [image-to-coco-json-converter](https://github.com/chrise96/image-to-coco-json-converter) git repository was used to
convert masks to COCO annotations. The create-custom-coco-dataset.ipynb and was edited
according to my needs. In the src/create_annotations.py were commented lines with segmentation, because our goal is to
compare bounding boxes, so segmentation is not needed. Closer information about this repository can be found in its own
README.md.

## scripts

#### analysis_tools.py

Functions for analysis of the datasets are provided there. Get defect count for each product type in DNAI dataset, get
number of images with at least one defect and without any defect, get number of annotations for each defect type.

#### conversions.py

Converting COCO annotations to Severstal CSV foramt and write it into CSV file are available funtions there.

#### evaluation.py

Functions for computing of Intersection and Intersection over Union (IoU) are provided there.

#### iou_metric.py

This file provides functions to compute number of detected defects, true positives and false positives according the IoU
threshold.

#### softer_metric.py

This file proveds functions to compute number of detected defects, true positives and false positives according to the
benevolent metric.

#### SeverstalJob.sh

Example script used on MetaCentrum to start training with YOLO.

## main

#### main.ipynb

Showing example main usage of helper functions in folder 'scripts'.

#### fiftyOne.ipynb

This notebook is showing how to view the ground truth annotations of dataset with performed predictions.

#### enc2mask.ipynb

Conversion of Severstal csv format to masks.

#### unet.ipynb

My edited version of the used solution from Kaggle on Severstal dataset.

