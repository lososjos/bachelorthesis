import json

from scripts.evaluation import get_iou


def get_tp_fp(path_result_json: str, path_dataset_gt_annotations: str, iou_thresholds: dict[int, int]):
	"""
			Process the given json files and returns number of TP and FP

			Args:
				path_result_json: str
					path to json with result annotations

				path_dataset_gt_annotations: dict
					path to json with ground truth annotations

				iou_thresholds: dict[int, int]
					dictionary mapping category id to its IoU threshold
			Returns:
				false_positive
					dictionary with calculated number of false positives for each defect type

				true_positive
					dictionary with calculated number of true positives for each defect type

				prediction_count
					checksum of TP and FP
	"""
	prediction_count = 0
	false_positive = dict()
	true_positive = dict()
	with open(path_result_json, "r") as f:
		data = json.load(
			f)  # data [{image_id: id, bbox:[x1, y1, width, height], score: conf, category_id: id, category_name: defect, segmentation: [], iscrowd: 0, area: width*height}, ...]
		with open(path_dataset_gt_annotations, "r") as gt_f:
			gt_data = json.load(gt_f)
			for i in range(len(gt_data["categories"])):
				false_positive[i] = 0
				true_positive[i] = 0
			# go through all predictions
			for prediction in data:
				prediction_count += 1
				best_iou = 0
				# for each prediction find any GT annotation and if the GT annotation which fits the best is below IoU threshold is marked as FP otherwise TP
				for annotation in gt_data["annotations"]:
					# skip annotations for different images
					if prediction["image_id"] != annotation["image_id"]:
						continue
					detection_bbox = prediction["bbox"]
					annotation_bbox = annotation["bbox"]
					detection_bbox = {"x1": detection_bbox[0], "y1": detection_bbox[1],
					                  "x2": detection_bbox[0] + detection_bbox[2],
					                  "y2": detection_bbox[1] + detection_bbox[3]}
					annotation_bbox = {"x1": annotation_bbox[0], "y1": annotation_bbox[1],
					                   "x2": annotation_bbox[0] + annotation_bbox[2],
					                   "y2": annotation_bbox[1] + annotation_bbox[3]}

					iou = get_iou(detection_bbox, annotation_bbox)
					if iou > best_iou and prediction["category_id"] == annotation["category_id"]:
						best_iou = iou
				if best_iou < iou_thresholds[prediction["category_id"]]:
					false_positive[prediction["category_id"]] += 1
				else:
					true_positive[prediction["category_id"]] += 1
	return false_positive, true_positive, prediction_count


def get_detected_count(path_result_json: str, path_dataset_gt_annotations: str, iou_thresholds: dict[int, int]):
	"""
			Process the given json files and returns number of detected defects.

			Args:
				path_result_json: str
					path to json with result annotations

				path_dataset_gt_annotations: dict
					path to json with ground truth annotations

				iou_thresholds: dict[int, int]
					dictionary mapping category id to its IoU threshold
			Returns:
				gt_categories
					categories in ground truth json

				gt_count
					number of ground truth annotations for each defect type

				detected_count
					number of detected ground truth annotations for each defect type
	"""
	detected_count = dict()
	gt_count = dict()
	with open(path_result_json, "r") as f:
		data = json.load(
			f)  # data [{image_id: id, bbox:[x1, y1, width, height], score: conf, category_id: id, category_name: defect, segmentation: [], iscrowd: 0, area: width*height}, ...]
		with open(path_dataset_gt_annotations, "r") as gt_f:
			gt_data = json.load(gt_f)
			gt_categories = gt_data["categories"]
			for i in range(len(gt_categories)):
				detected_count[i] = 0
				gt_count[i] = 0

			# go through all GT annotation a get the best one, if IoU is more than threshold, set this detection as TP
			for annotation in gt_data["annotations"]:
				gt_count[annotation["category_id"]] += 1
				best_iou = 0
				# for each GT annotation try if any or which prediction fit the GT annotation and find which fit the best to the GT annotation, if the best fit is above IoU threshold the GT annotation is marked as detected.
				for prediction in data:
					if prediction["image_id"] != annotation["image_id"]:
						continue
					detection_bbox = prediction["bbox"]
					annotation_bbox = annotation["bbox"]
					detection_bbox = {"x1": detection_bbox[0], "y1": detection_bbox[1],
					                  "x2": detection_bbox[0] + detection_bbox[2],
					                  "y2": detection_bbox[1] + detection_bbox[3]}
					annotation_bbox = {"x1": annotation_bbox[0], "y1": annotation_bbox[1],
					                   "x2": annotation_bbox[0] + annotation_bbox[2],
					                   "y2": annotation_bbox[1] + annotation_bbox[3]}

					iou = get_iou(detection_bbox, annotation_bbox)
					if iou > best_iou and prediction["category_id"] == annotation["category_id"]:
						best_iou = iou

				if best_iou >= iou_thresholds[annotation["category_id"]]:
					detected_count[annotation["category_id"]] += 1
	return gt_categories, gt_count, detected_count


def write_metrics(dst_path: str, gt_categories: list, gt_count: dict, detected_count: dict, true_positive: dict,
                  false_positive: dict, prediction_count: int):
	"""
			Write calculated metrics into dst_path.

			Args:
				dst_path: str
					path to output txt file

				gt_categories: list
					available categories in ground truth json

				gt_count: dict[int, int]
					number of ground truth annotations for each defect type

				detected_count: dict
					number of detected defects for each defect type

				true_positive: dict
					number of TP for each defect type

				false_positive: dict
					number of FP for each defect type

				prediction_count: int
					number of predictions in result json
			Returns:
				None
	"""
	sum_detected = 0
	sum_gt = 0
	sum_false_positives = 0
	sum_true_positives = 0
	with open(dst_path, "a") as out:
		for category in gt_categories:
			if gt_count[category["id"]] == 0:
				continue
			print("# " + category["name"])
			out.write("# " + category["name"] + "\n")
			print(str(detected_count[category["id"]]) + "/" + str(gt_count[category["id"]]) + " of " + category[
				"name"] + "(" + str(
				round((detected_count[category["id"]] / gt_count[category["id"]]) * 100, 2)) + " %)")
			out.write(str(detected_count[category["id"]]) + "/" + str(gt_count[category["id"]]) + " of " + category[
				"name"] + "(" + str(
				round((detected_count[category["id"]] / gt_count[category["id"]]) * 100, 2)) + " %)\n")
			sum_detected += detected_count[category["id"]]
			sum_gt += gt_count[category["id"]]
			print("false positive " + str(false_positive[category["id"]]))
			out.write("false positive " + str(false_positive[category["id"]]) + "\n")
			sum_false_positives += false_positive[category["id"]]
			sum_true_positives += true_positive[category["id"]]
		print("Sum of detected " + str(sum_detected) + "/" + str(sum_gt) + "(" + str(
			round((sum_detected / sum_gt) * 100, 2)) + " %)")
		out.write("Sum of detected " + str(sum_detected) + "/" + str(sum_gt) + "(" + str(
			round((sum_detected / sum_gt) * 100, 2)) + " %)\n")
		print("Sum of true positives is " + str(sum_true_positives))
		out.write("Sum of true positives is " + str(sum_true_positives) + "\n")
		print("Sum of false positives is " + str(sum_false_positives))
		out.write("Sum of false positives is " + str(sum_false_positives) + "\n")
		print("Sum of predictions is " + str(prediction_count))
		out.write("Sum of predictions is " + str(prediction_count) + "\n")


def print_write_metrics(path_result_json: str,
                        path_dataset_gt_annotations: str,
                        dst_path: str,
                        iou_thresholds: dict[int, int]):
	"""
			Print and write calculated number of detected defects, TP and FP according the given IoU thresholds.

			Args:
				path_result_json: str
					path to json with result annotations

				path_dataset_gt_annotations: dict
					path to json with ground truth annotations

				dst_path: str
					path to output txt file

				iou_thresholds: dict[int, int]
					dictionary mapping category id to its IoU threshold

			Returns:
				None
		"""

	# Calculate the count of detected defects
	gt_categories, gt_count, detected_count = get_detected_count(path_result_json=path_result_json,
	                                                             path_dataset_gt_annotations=path_dataset_gt_annotations,
	                                                             iou_thresholds=iou_thresholds)
	# Calculate False Positives and True Positives
	false_positive, true_positive, prediction_count = get_tp_fp(path_result_json=path_result_json,
	                                                            path_dataset_gt_annotations=path_dataset_gt_annotations,
	                                                            iou_thresholds=iou_thresholds)
	# Print counts of TP and FP
	write_metrics(dst_path=dst_path, gt_categories=gt_categories, gt_count=gt_count, detected_count=detected_count,
	              true_positive=true_positive, false_positive=false_positive, prediction_count=prediction_count)
