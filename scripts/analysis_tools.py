import json

def prnit_for_each_product_type_defects_counts(coco_json_path: str, types=None):
	"""
	Print the annotation count of each defect tpye per product type in DNAI dataset

	Args:
		coco_json_path : str
			path to COCO json file with annotations
		types : set (default=None)
			 product types in the dataset
	Returns:
		None
	"""
	# types from DNAI dataset
	if types is None:
		types = {"VBMT160402E-FM_T9415", "DCMT11T308E-FM_T7325", "BPV-WNMG080408-M3-362", "TNMG160404ER-SI_T9325",
		         "S-RDMW10-001609-P01_M8330", "CCGT09T304EL-SI_T8330_", "CNMG120408E-NM_T7335", "TCMT16T304E-FM_T8430",
		         "CCGT120404F-AL_HF7", "SNMG120408E-M_T5315", "CNMG190616E-RM_T9315", "SNMG250924E-RM_T9226",
		         "S-VCGX220530F-AL_HF7", "VNMG160404E-M_T9325", "BPV-WNMG080408-M3-362", "RT160608ER-11_5020",
		         "P-CNMG120408E-SM_T8330", "S-CNMG120408-SIH_T8330", "CNMM250924E-77_T9335", "SNMX19-R30_6640",
		         "ZP32ER-RM8345", "P-RCMX3209MO-RM2_T9310", "S-TNMM330924E-RM_T9335", "S-CCMT060204E-NF2_9605",
		         "TCGT110204EL-SI_T8430", "S-APMT1135PDERR08_G8330", "TCMT110204E-FM_T843", "RT100308R-81_5020",
		         "S-APKT100308ER-M_M8340"}

	with open(coco_json_path, "r") as f:
		data = json.load(f)
		categories = dict()
		imgIdsToType = dict()
		typeToImgIds = dict()
		typesToAnnotations = dict()
		typesToCount = dict()
		# init
		for type in types:
			typesToAnnotations[type] = dict()
			typesToCount[type] = dict()
			typeToImgIds[type] = []

		for img in data["images"]:
			for type in types:
				if type in img["file_name"] and not "-P-" + type in img["file_name"]:
					imgIdsToType[img["id"]] = type
					break

		for category in data["categories"]:
			categories[category["id"]] = category["name"]
		# init
		for type in types:
			for defectName in categories.values():
				typesToAnnotations[type][defectName] = 0
				typesToCount[type][defectName] = set()

		for annot in data["annotations"]:
			type = imgIdsToType[annot["image_id"]]
			defectName = categories[annot["category_id"]]
			typesToAnnotations[type][defectName] += 1
			typesToCount[type][defectName].add(annot["image_id"])

	for type in types:
		print("----------------------------------------------------")
		print(type)
		for defectName in categories.values():
			print(str(defectName) + ": " + str(typesToAnnotations[type][defectName]))

	print("########################################################")

	for type in types:
		print("----------------------------------------------------")
		for defectName in categories.values():
			if defectName == "errors":
				print(type)
			else:
				print(str(defectName) + ": " + str(len(typesToCount[type][defectName])))

	sums = dict()
	for cat in categories.values():
		sums[cat] = 0

	for type in types:
		for defectName in categories.values():
			sums[defectName] += typesToAnnotations[type][defectName]

	for cat in categories.values():
		print(sums[cat])


def print_images_count_with_or_without_any_defect(coco_json_path: str):
	"""
	Print count of images with at least one annotation and count of images without any annotation for the dataset in the given json file.

	Args:
		coco_json_path : str
			path to COCO json file with images and annotations

	Returns:
		None
	"""
	with open(coco_json_path, "r+") as f:
		data = json.load(f)
		categories = dict()
		counts = dict()
		cat_names = dict()
		imgs = dict()
		for img in data["images"]:
			imgs[img["id"]] = 0
		for anot in data["annotations"]:
			imgs[anot["image_id"]] += 1
		imgs_without = 0
		imgs_with = 0
		for count in imgs.values():
			if count > 0:
				imgs_with += 1
			else:
				imgs_without += 1
		print("img count: " + str(len(imgs)))
		print("imgs without any defect count: " + str(imgs_without))
		print("imgs with at least one defect: " + str(imgs_with))


def print_defects_counts(coco_json_path: str):
	"""
	Print count of annotations for each category in the given COCO json.

	Args:
		coco_json_path : str
			path to COCO json file with images and annotations

	Returns:
		None
	"""
	with open(coco_json_path, "r+") as f:
		data = json.load(f)
		categories = dict()
		counts = dict()
		cat_names = dict()
		for i in data["categories"]:
			categories[i["id"]] = 0.0
			cat_names[i["id"]] = i["name"]
			counts[i["id"]] = 0
		for anot in data["annotations"]:
			categories[anot["category_id"]] += anot["area"]
			counts[anot["category_id"]] += 1

		for category, value in zip(categories.keys(), categories.values()):
			print(cat_names[category])
			print(counts[category])
