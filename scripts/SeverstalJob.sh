#!/bin/bash
#PBS -N severstal_steel_slice_256_005_00_max
#PBS -l select=1:ncpus=2:ngpus=1:mem=40gb:scratch_local=40gb:cluster=gita
#PBS -l walltime=24:00:00
#PBS -m abe

# define a DATADIR variable: directory where the input files are taken from and where output will be copied to
DATADIR=/storage/praha1/home/lososjos # substitute username and path to to your real username and path
DATASET=severstal-steel-defect-detection
# append a line to a file "jobs_info.txt" containing the ID of the job, the hostname of node it is run on and the path to a scratch directory
# this information helps to find a scratch directory in case the job fails and you need to remove the scratch directory manually 
echo "$PBS_JOBID is running on node `hostname -f` in a scratch directory $SCRATCHDIR" >> $DATADIR/jobs_info.txt

# test if scratch directory is set
# if scratch directory is not set, issue error message and exit
test -n "$SCRATCHDIR" || { echo >&2 "Variable SCRATCHDIR is not set!"; exit 1; }

# copy input file "h2o.com" to scratch directory
# if the copy operation fails, issue error message and exit
cp -r $DATADIR/datasets/$DATASET/slice_256_005_00_max $SCRATCHDIR/$DATASET || { echo >&2 "Error while copying input file(s)!"; exit 2; }

# copy same already downloaded version of YOLO
# cp r $DATADIR/yolov5 $SCRATCHDIR/yolov5 || { echo >&2 "Error while copying yolo file(s)!"; exit 2; }

# move into scratch directory
cd $SCRATCHDIR

# clone YOLOv5 if not downloaded yet
git clone https://github.com/ultralytics/yolov5 || { echo >&2 "Error while copying yolo from git!"; exit 3; }
cd yolov5

# better to install manually during interactive session
# python3.9 -m pip install -r requirements.txt || { echo >&2 "Error while installing requirements!"; exit 4; }

# training on images with different width and height
#python3.9 train.py --img 800 --rect --batch 32 --epochs 1000 --data ../$DATASET/data.yaml --weights '' --cfg yolov5m.yaml --cache ram --patience 100 || { echo >&2 "Error while training!"; exit 5; }

# begin train
python3.9 train.py --img 800 --rect --batch 32 --epochs 1000 --data ../$DATASET/data.yaml --weights '' --cfg yolov5m.yaml --cache ram --patience 100 || { echo >&2 "Error while training!"; exit 5; }

# copy results
cd ..
cp -r $SCRATCHDIR/yolov5/runs/train $DATADIR/results/$PBS_JOBID || { echo >&2 "Result file(s) copying failed (with a code $?) !!"; exit 6; }

# clean the SCRATCH directory
clean_scratch