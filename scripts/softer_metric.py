import json

from scripts.evaluation import get_intersection


def get_detected_count_softer(path_result_json: str, path_gt_json: str,
                              smaller_prediction_threshold=0.75, larger_prediction_threshold=0.75):
	"""
				Process the given json files and returns number of correctly detected defects according to given ground truth.

				Args:
					path_result_json: str
						path to json with result annotations

					path_gt_json: dict
						path to json with ground truth annotations

					smaller_prediction_threshold: float
						hyperparameter (default 0.75) which says how large must be ratio of intersection of prediction bbox smaller than ground truth bbox

					larger_prediction_threshold: float
						hyperparameter (default 0.75) which says how large must be ratio of intersection of prediction bbox larger than ground truth bbox

				Returns:
					prediction_count
						number of predictions in path_result_json

					gt_annotations_isDetected
						dictionary with annotations ids as keys and boolean indicating if is detected

	"""
	gt_annotations_isDetected = dict()  # {ann_id: isDetected}
	with open(path_result_json, "r") as f:
		data = json.load(
			f)  # data [{image_id: id, bbox:[x1, y1, width, height], score: conf, category_id: id, category_name: defect, segmentation: [], iscrowd: 0, area: width*height}, ...]
		with open(path_gt_json, "r") as gt_f:
			gt_data = json.load(gt_f)
			prediction_count = len(data)
			# go through all GT annotation and find out if is detected according to the intersection criteria
			for annotation in gt_data["annotations"]:
				gt_annotations_isDetected[annotation["id"]] = False  # init defect as not detected
				# for each annotation try all detections if fit any defect
				for prediction in data:
					# skip detections for different images
					if prediction["image_id"] != annotation["image_id"]:
						continue
					detection_bbox = prediction["bbox"]
					annotation_bbox = annotation["bbox"]
					prediction_area = prediction["area"]
					defect_area = annotation["area"]
					detection_bb = {"x1": detection_bbox[0], "y1": detection_bbox[1],
					                "x2": detection_bbox[0] + detection_bbox[2],
					                "y2": detection_bbox[1] + detection_bbox[3]}
					annotation_bb = {"x1": annotation_bbox[0], "y1": annotation_bbox[1],
					                 "x2": annotation_bbox[0] + annotation_bbox[2],
					                 "y2": annotation_bbox[1] + annotation_bbox[3]}

					intersection = get_intersection(detection_bb, annotation_bb)
					if prediction_area < defect_area:
						# maybe add that the prediction must be greater than some % of defect area
						isDetected = True if (intersection >= smaller_prediction_threshold * prediction_area) \
						                     and prediction["category_id"] == annotation["category_id"] else False
					else:
						isDetected = True if (intersection > larger_prediction_threshold * defect_area) \
						                     and prediction["category_id"] == annotation["category_id"] else False
					if not gt_annotations_isDetected[annotation["id"]]:
						gt_annotations_isDetected[annotation["id"]] = isDetected
	return prediction_count, gt_annotations_isDetected


def get_tp_fp_softer(path_result_json: str, path_gt_json: str, smaller_prediction_threshold=0.75,
                     larger_prediction_threshold=0.75):
	"""
			Process the given json files and returns number of TP and FP

			Args:
				path_result_json: str
					path to json with result annotations

				path_gt_json: dict
					path to json with ground truth annotations

				smaller_prediction_threshold: float
					hyperparameter (default 0.75) which says how large must be ratio of intersection of prediction bbox smaller than ground truth bbox

				larger_prediction_threshold: float
					hyperparameter (default 0.75) which says how large must be ratio of intersection of prediction bbox larger than ground truth bbox

			Returns:
				tp_count
					number of true positives in result json according to ground truth json

				fp_count
					number of false positives in result json according to ground truth json

	"""
	# init counts
	tp_count = 0
	fp_count = 0
	with open(path_result_json, "r") as f:
		data = json.load(
			f)  # data [{image_id: id, bbox:[x1, y1, width, height], score: conf, category_id: id, category_name: defect, segmentation: [], iscrowd: 0, area: width*height}, ...]
		with open(path_gt_json, "r") as gt_f:
			gt_data = json.load(gt_f)
			# go through all predictions and check if it is TP for any GT annotation
			for prediction in data:
				isValidPrediction = False
				# for each prediction
				for annotation in gt_data["annotations"]:
					# skip detections for different images
					if prediction["image_id"] != annotation["image_id"]:
						continue
					detection_bbox = prediction["bbox"]
					annotation_bbox = annotation["bbox"]
					prediction_area = prediction["area"]
					defect_area = annotation["area"]
					detection_bb = {"x1": detection_bbox[0], "y1": detection_bbox[1],
					                "x2": detection_bbox[0] + detection_bbox[2],
					                "y2": detection_bbox[1] + detection_bbox[3]}
					annotation_bb = {"x1": annotation_bbox[0], "y1": annotation_bbox[1],
					                 "x2": annotation_bbox[0] + annotation_bbox[2],
					                 "y2": annotation_bbox[1] + annotation_bbox[3]}

					intersection = get_intersection(detection_bb, annotation_bb)
					if prediction_area < defect_area:
						# maybe add that the prediction must be greater than some % of defect area
						isValidPrediction = True if (intersection >= smaller_prediction_threshold * prediction_area) \
						                            and prediction["category_id"] == annotation[
							                            "category_id"] else False
					else:
						isValidPrediction = True if (intersection > larger_prediction_threshold * defect_area) \
						                            and prediction["category_id"] == annotation[
							                            "category_id"] else False
					if isValidPrediction:
						tp_count += 1
						break
				if not isValidPrediction:
					fp_count += 1
	return tp_count, fp_count


def write_softer_metrics(dst_path: str, gt_annotations_isDetected: dict, tp_count: int, fp_count: int,
                         prediction_count: int):
	"""
			Write given args as true positives, false positives and number of detected defects into dst_path.

			Args:
				dst_path: str
					path to the output file

				gt_annotations_isDetected: dict
					dictionary with key of id for each prediction annotation and assigned it True if is detected, otherwise False

				tp_count: int
					number of true positive predictions

				fp_count: int
					number of false positive predictions

				prediction_count:
					number of all prediction,secondary purpose is checksum for TP and FP

			Returns:
				None
	"""
	detected = 0
	for isDetected in gt_annotations_isDetected.values():
		detected += int(isDetected)
		if len(gt_annotations_isDetected) == 0:
			break
	with open(dst_path, "a") as out:
		print("The number of detected defect is " + str(detected) + " out of " + str(
			len(gt_annotations_isDetected)) + "(" + str(
			round(detected / len(gt_annotations_isDetected) * 100, 2)) + " %)")
		out.write("The number of detected defect is " + str(detected) + " out of " + str(
			len(gt_annotations_isDetected)) + "(" + str(
			round(detected / len(gt_annotations_isDetected) * 100, 2)) + " %)\n")
		print("The number of true positives is " + str(tp_count))
		out.write("The number of true positives is " + str(tp_count) + "\n")
		print("The number of false positives is " + str(fp_count))
		out.write("The number of false positives is " + str(fp_count) + "\n")
		print("Sum of predictions is " + str(prediction_count))
		out.write("Sum of predictions is " + str(prediction_count) + "\n")


def print_write_softer_metrics(path_result_json: str, path_gt_json: str, dst_path: str):
	"""
		Calculate then print and write into dst_path the count of TP and FP according the softer metric, which use ratio of intersection according to size of prediction or defect.

		Args:
			path_result_json : str
				path to json file with array of COCO annotations

			path_gt_json : str
				path to COCO json file with annotations and images

			dst_path: str
				path to output file

		Returns:
			None
	"""
	# smaller_prediction_threshold = 0.75
	# larger_prediction_threshold = 0.75

	# get number of detected ground truth defects
	prediction_count, gt_annotations_isDetected = get_detected_count_softer(path_result_json=path_result_json,
	                                                                        path_gt_json=path_gt_json)
	# get TP a FP
	tp_count, fp_count = get_tp_fp_softer(path_result_json=path_result_json, path_gt_json=path_gt_json)

	write_softer_metrics(dst_path=dst_path, gt_annotations_isDetected=gt_annotations_isDetected,
	                     tp_count=tp_count, fp_count=fp_count, prediction_count=prediction_count)
