import json

def get_intersection(bb1, bb2):
	"""
	   Calculate the Intersection of two bounding boxes.

	   Args:
		   bb1 : dict
			   Keys: {'x1', 'x2', 'y1', 'y2'}
			   The (x1, y1) position is in the top left corner,
			   the (x2, y2) position is in the bottom right corner
		   bb2 : dict
			   Keys: {'x1', 'x2', 'y1', 'y2'}
			   The (x, y) position is in the top left corner,
			   the (x2, y2) position is in the bottom right corner

	   Returns:
	    float - area of intersection
	   """
	assert bb1['x1'] < bb1['x2']
	assert bb1['y1'] < bb1['y2']
	assert bb2['x1'] < bb2['x2']
	assert bb2['y1'] < bb2['y2']

	# determine the coordinates of the intersection rectangle
	x_left = max(bb1['x1'], bb2['x1'])
	y_top = max(bb1['y1'], bb2['y1'])
	x_right = min(bb1['x2'], bb2['x2'])
	y_bottom = min(bb1['y2'], bb2['y2'])

	if x_right < x_left or y_bottom < y_top:
		return 0.0

	intersection_area = (x_right - x_left) * (y_bottom - y_top)
	return intersection_area


def get_iou(bb1, bb2):
	"""
	Calculate the Intersection over Union (IoU) of two bounding boxes.

	Args:
		bb1 : dict
			Keys: {'x1', 'x2', 'y1', 'y2'}
			The (x1, y1) position is in the top left corner,
			the (x2, y2) position is in the bottom right corner

		bb2 : dict
			Keys: {'x1', 'x2', 'y1', 'y2'}
			The (x, y) position is in the top left corner,
			the (x2, y2) position is in the bottom right corner

	Return:
		float in [0, 1] - intersection over overlap value
	"""
	# The intersection of two axis-aligned bounding boxes is always an
	# axis-aligned bounding box
	intersection_area = get_intersection(bb1, bb2)

	# compute the area of both AABBs
	bb1_area = (bb1['x2'] - bb1['x1']) * (bb1['y2'] - bb1['y1'])
	bb2_area = (bb2['x2'] - bb2['x1']) * (bb2['y2'] - bb2['y1'])

	# compute the intersection over union by taking the intersection
	# area and dividing it by the sum of prediction + ground-truth
	# areas - the interesection area
	iou = intersection_area / float(bb1_area + bb2_area - intersection_area)
	assert iou >= 0.0
	assert iou <= 1.0
	return iou


def add_score_field_to_prediction(src_result_json: str, dst_result_json: str, score_value=0.5):
	"""
		Add field 'score' into predicted annotations to be able to use dst_result_json for evaluation with SAHI.

		Args:
			src_result_json: str
				path to json with result annotations without field 'score'

			dst_result_json: str
				path to output json

			score_value: float (default value = 0.5)
				value of score to be inserted

		Return:
			None
	"""
	updated_anns = []
	with open(src_result_json, "r") as f:
		data = json.load(f)
		for ann in data:
			ann["score"] = score_value
			updated_anns.append(ann)
	with open(dst_result_json, "w") as ff:
		json.dump(updated_anns, ff)
