import numpy as np
import pandas as pd
from pycocotools.coco import COCO

def mask2rle(mask):
    """
    		Convert mask into RLE.

    		Args:
    			mask: ndarray
    				binary mask (numpy 2D array)

    		Return:
    			str - RLE
    """
    pixels = mask.T.flatten()
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[:-1:2]
    return ' '.join(str(x) for x in runs)


def convert_coco2csv(coco: COCO, category_count: int, ImageIds=None, ClassIds=None, EncodedPixels=None):
    """
            Convert COCO annotations into Severstal CSV format.

            Args:
                coco: COCO
                    COCO(path/to/json/to/convert)

                category_count: int
                    number of categories

                ImageIds: list (default=None)
                    image ids to be added to dataset

                ClassIds: list (default=None)
                    classes ids to be added to dataset

                EncodedPixels: list (default=None)
                    encoded pixels to be added to dataset

            Return:
                ImageIds
                    updated ImageIds from input

                ClassIds
                    updated ClassIds from input

                EncodedPixels
                    updated EncodedPixels from input
    """
    if EncodedPixels is None:
        EncodedPixels = []
    if ClassIds is None:
        ClassIds = []
    if ImageIds is None:
        ImageIds = []
    img_count = len(coco.imgs)
    # init
    anns2imgs = dict()
    for i in range(img_count):
        anns2imgs[i] = dict()
        for j in range(category_count):
            anns2imgs[i][j] = []

    for image_id in range(img_count):
        img = coco.imgs[image_id]
        cat_ids = coco.getCatIds()
        anns_ids = coco.getAnnIds(imgIds=img['id'], catIds=cat_ids, iscrowd=None)
        anns = coco.loadAnns(anns_ids)
        # anns2imgs = {image_id: {category_id: [anns for image_id of category_id] }}
        for ann in anns:
            anns2imgs[ann["image_id"]][ann["category_id"]] = np.append(anns2imgs[ann["image_id"]][ann["category_id"]],
                                                                       ann)

    for i in range(img_count):
        img = coco.imgs[i]
        for j in range(category_count):
            mask = np.zeros((img["height"], img["width"]))

            for ann in anns2imgs[i][j]:
                # create segmentation from bounding box if segmentation does not exist
                if not 'segmentation' in ann or ann['segmentation'] == []:
                    bb = ann['bbox']
                    x1, x2, y1, y2 = [bb[0], bb[0] + bb[2], bb[1], bb[1] + bb[3]]
                    ann['segmentation'] = [[x1, y1, x1, y2, x2, y1, x2, y2]]
                mask += coco.annToMask(ann)
            rle = mask2rle(mask[:, :])
            if len(rle) > 0:
                ImageIds = np.append(ImageIds, img["file_name"])
                ClassIds = np.append(ClassIds, int(j))
                EncodedPixels = np.append(EncodedPixels, rle)
        print("Doing " + str(i) + "/" + str(img_count - 1))
    return ImageIds, ClassIds, EncodedPixels


def write_csv_annotations(ImageIds: list, ClassIds: list, EncodedPixels: list, dst_csv_path: str):
    """
                Convert COCO annotations into Severstal CSV format.

                Args:
                    ImageIds: list
                        image ids to be added to dataset

                    ClassIds: list
                        classes ids to be added to dataset

                    EncodedPixels: list
                        encoded pixels to be added to dataset

                    dst_csv_path: str
                        path to output csv file with annotations

                Return:
                    None
        """
    d = {'ImageId': ImageIds, 'ClassId': ClassIds, 'EncodedPixels': EncodedPixels}
    df = pd.DataFrame(data=d)
    print(df)
    df.to_csv(dst_csv_path, index=False)
